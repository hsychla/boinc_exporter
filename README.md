# boinc_exporter.py

boinc_exporter.py is a small python 3 program than parses the [BOINC client](https://boinc.berkeley.edu/) state XML file (typically located in /var/lib/boinc-client/client_state.xml) and transforms many of the statistics contained within into the .prom format that the [Prometheus monitoring server](https://prometheus.io/) can ingest.

Its development was focused on simplicity, resiliency, portability and ease-of-use.

## Installation

Just copy the file boinc_exporter.py in a suitable directory (like ~/bin or /usr/local/bin) in your system and run it form there.

After having battled resolving library dependencies many times in the past, I chose to avoid having many in this program, to make it as simple as possible to install and use.

### Integration with Prometheus
boinc_exporter.py is not a fully-fledged prometheus exporter. It does not start listening on some TCP port for HTTP requests like normal prometheus exporters do and therefore Prometheus cannot scrape it directly.

Instead, it is meant to be used as a textfile collector with [node_exporter](https://github.com/prometheus/node_exporter). This typically means that you need to setup a systemd timer or a cron job on the machine(s) running BOINC, which executes something like the following every X minutes:

```sh
$ /usr/local/bin/boinc_exporter.py | sponge /var/lib/node-exporter/boinc_exporter.prom
```

node-exporter can then pickup the contents of the file /var/lib/node-exporter/boinc_exporter.prom and relay them to Prometheus along with its own metrics.

Setting up textfile collectors for node_exporter is beyond the scope of this document. You can find more information here:
- https://github.com/prometheus-community/node-exporter-textfile-collector-scripts
- https://www.robustperception.io/using-the-textfile-collector-from-a-shell-script

## Usage

boinc_exporter.py provides an informational help screen, if asked:

```sh
$ /usr/local/bin/boinc_exporter.py --help
boinc_exporter.py version 1.2. Copyright Thomas Venieris 2021-2022
GNU General Public License version 3 release

Usage: /usr/local/bin/boinc_exporter.py -f <client_state.xml>

Parameters:
-h, --help                          This text
-f, --file <client_state.xml>       Specify an alternative path for the file
                                    client_state.xml. Default location is:
                                    /var/lib/boinc-client/client_state.xml

    --no-hostinfo                   Disables hostinfo statistics
    --no-timestats                  Disables time statistics
    --no-netstats                   Disables network statistics
    --no-projects                   Disables project statistics
    --no-workunits                  Disables workunit statistics
    --no-results                    Disables result statistics
    --no-active-tasks               Disables active task statistics
    --no-user                       Disables user and client statistics
```

### Options to disable specific types of metrics
The client_state.xml file contains many different statistic metrics in distinct sections. boinc_exporter.py exposes all of those statistics by default.

If your Prometheus installation storage is limited, or there are certain metrics you do not care about, you can disable their generation by using one or more of the "--no-something" options shown in the help screen above.

### Normal operation

A small portion of the metrics provided by boinc_exporter.py is shown below as an example. To see the full output of the program you can simply run boinc_exporter.py or look into the contents of [misc/boinc_exporter.prom](misc/boinc_exporter.prom).

```sh
$ ./boinc_exporter.py
# HELP boinc_scrape_errors Number of errors encountered while parsing the boinc xml file
# TYPE boinc_scrape_errors gauge
boinc_scrape_errors 0

# HELP boinc_hostinfo_proc_cpus Number of processors, vendor and model details
# TYPE boinc_hostinfo_proc_cpus gauge
boinc_hostinfo_proc_cpus{vendor="ARM",model="BCM2835 [Impl 0x41 Arch 8 Variant 0x0 Part 0xd08 Rev 3]",features="fp asimd evtstrm crc32 cpuid"} 4

# HELP boinc_hostinfo_mem_nbytes Available processor memory, in bytes
# TYPE boinc_hostinfo_mem_nbytes gauge
boinc_hostinfo_mem_nbytes 4294966272

# HELP boinc_project_rpc_seqno Number of Remote Procedure Calls
# TYPE boinc_project_rpc_seqno counter
boinc_project_rpc_seqno{project="Einstein@Home"} 131
boinc_project_rpc_seqno{project="LHC@home"} 1528
boinc_project_rpc_seqno{project="Rosetta@home"} 1084
boinc_project_rpc_seqno{project="Universe@Home"} 3769
boinc_project_rpc_seqno{project="World Community Grid"} 125

# HELP boinc_project_host_total_credit Total credits earned by host per project
# TYPE boinc_project_host_total_credit gauge
boinc_project_host_total_credit{project="Einstein@Home"} 0
boinc_project_host_total_credit{project="LHC@home"} 23266.842788
boinc_project_host_total_credit{project="Rosetta@home"} 51629.167285
boinc_project_host_total_credit{project="Universe@Home"} 0
boinc_project_host_total_credit{project="World Community Grid"} 0

# HELP boinc_project_nrpc_failures Number of Remote Procedure Call failures
# TYPE boinc_project_nrpc_failures counter
boinc_project_nrpc_failures{project="Einstein@Home"} 0
boinc_project_nrpc_failures{project="LHC@home"} 0
boinc_project_nrpc_failures{project="Rosetta@home"} 0
boinc_project_nrpc_failures{project="Universe@Home"} 0
boinc_project_nrpc_failures{project="World Community Grid"} 0

# HELP boinc_workunit_count Number of workunits per application
# TYPE boinc_workunit_count gauge
boinc_workunit_count{app="sixtrack",ver="50205"} 4
boinc_workunit_count{app="rosetta",ver="420"} 6

# HELP boinc_active_task_state 0=Paused, 1=Executing, 9=Suspended
# TYPE boinc_active_task_state gauge
boinc_active_task_state{url="https://lhcathome.cern.ch/lhcathome/",name="hl14_0000095_5_6231_6032_64656311_0",version_num="50205"} 1
boinc_active_task_state{url="https://boinc.bakerlab.org/rosetta/",name="P23_nb0016_c3r3_solv46_c107_0001_108",version_num="420"} 1
boinc_active_task_state{url="https://boinc.bakerlab.org/rosetta/",name="2011061_0001_0001_1058768_1447_0",version_num="420"} 1

# HELP boinc_active_task_chkp_elapsed_time Number of seconds elapsed since task became active
# TYPE boinc_active_task_chkp_elapsed_time gauge
boinc_active_task_chkp_elapsed_time{url="https://lhcathome.cern.ch/lhcathome/",name="hl14_0000095_5_6231_6032_64656311_0",version_num="50205"} 33500.071905
boinc_active_task_chkp_elapsed_time{url="https://boinc.bakerlab.org/rosetta/",name="P23_nb0016_c3r3_solv46_c107_0001_108",version_num="420"} 4256.75169
boinc_active_task_chkp_elapsed_time{url="https://boinc.bakerlab.org/rosetta/",name="2011061_0001_0001_1058768_1447_0",version_num="420"} 891.15706

# HELP boinc_active_task_chkp_fraction_done Percentage of task completed (0.0=0%, 1.0=100%)
# TYPE boinc_active_task_chkp_fraction_done gauge
boinc_active_task_chkp_fraction_done{url="https://lhcathome.cern.ch/lhcathome/",name="hl14_0000095_5_6231_6032_64656311_0",version_num="50205"} 0.685007
boinc_active_task_chkp_fraction_done{url="https://boinc.bakerlab.org/rosetta/",name="P23_nb0016_c3r3_solv46_c107_0001_108",version_num="420"} 0.322329
boinc_active_task_chkp_fraction_done{url="https://boinc.bakerlab.org/rosetta/",name="2011061_0001_0001_1058768_1447_0",version_num="420"} 0.012822
```

### A note on active task names

BOINC task names are sometimes quite long, often exceeding 100 characters. Most of Rosetta@home's work-units for example are famous for containing strings like "SAVE_ALL_OUT_IGNORE_THE_REST" in their names. This is a problem for Prometheus, because Prometheus likes numbers, text not so much. Additionally, large task names easily mess-up any Grafana dashboards that attempt to show them.

boinc_exporter.py tries to solve this problem by condensing task names to a limit of 36 characters. It does so by shortening the name and appending a suffix with a small hash of the original name. In some cases, this retains some association with the original name.

I have tested this code with the following BOINC projects and it seems to be working reasonably well. I can only hope it does not cause signigicant problems with projects I haven't tested.
- Asteroids@home
- Einstein@home
- LHC@home
- Rosetta@home
- Universe@Home
- World Community Grid

### You'll know if something breaks

In most cases, boinc_exporter.py will respond in the [Prometheus](https://prometheus.io/) .prom format, even when it runs into trouble. If it does not find the XML file, for example:

```sh
$ ./boinc_exporter.py
# HELP boinc_scrape_errors Number of errors encountered while parsing the boinc xml file
# TYPE boinc_scrape_errors gauge
boinc_scrape_errors{msg="File \'/var/lib/boinc-client/client_state.xml\' not found"} 1
```

This is signficant because you can then configure Prometheus to check if the "boinc_scrape_errors" is greater than 0, as we'll see later in the Prometheus alert rules examples. This will generate Prometheus alerts and, if your prometheus setup is using [alertmanager](https://prometheus.io/docs/alerting/latest/alertmanager/), it can go through your normal incident escallation process and wake up someone in the middle of the night. That's of course if your BOINC operation is so important for you.

"boinc_scrape_errors" are also generated if certain XML tags are not found in the XML file. This means that if the format of the XML changes for any reason and cannot be parsed, boinc_exporter.py will still collect whatever it can and just increase the "boinc_scrape_errors" metric for any statistics that it couldn't scrape.

In a nutshell, if "boinc_scrape_errors" keeps a value of "0", you can be pretty confident that all the statistics are being scraped successfully.

## Grafana dashboard

The official BOINC client makes a good effort to show the operator the state of the BOINC client, but it can't possibly compete with the marvelous pastel-laced goodness of [Grafana dashboards](https://grafana.com/).

If you are using Grafana to visualize your prometheus metrics, you can import the contents of the file [grafana/boinc_exporter_dashboard.json](grafana/boinc_exporter_dashboard.json) into your Grafana instance. This will allow you to almost instantly view most of the metrics transcoded by boinc_exporter.py in a way that is much more human friendly.

A few screenshots are included in the [grafana/screenshots/](grafana/screenshots/) directory to give you an idea what to expect:
![Active boinc tasks](grafana/screenshots/boinc_exporter_grafana_dashboard.png)
![Attached boinc projects](grafana/screenshots/boinc_exporter_grafana_dashboard_projects.png)
![Earned boinc credits](grafana/screenshots/boinc_exporter_grafana_dashboard_credits.png)

The high customizability of Grafana allows you to modify this dashboard according to any special needs and of course use it as inpiration to create your own.

## Prometheus alert rules

As already mentioned, you can configure Prometheus to monitor the metrics provided by boinc_exporter.py and generate alerts when certain thresholds are exceeded. These can then be picked up by [alertmanager](https://prometheus.io/docs/alerting/latest/alertmanager/) which will in turn send an e-mail, a Slack message or escalate to Pagerduty, Opsgenie or whatever you have it configure to do in such cases.

A few examples of such Prometheus alert rules are contained in the file [prometheus/alert_rules.yml](prometheus/alert_rules.yml)

Nobody's alerting requirements are the same, so I encourage you to study the rules contained in this file and adjust them to your needs before importing them in your setup. You can of course also use them as a starting point to develop new ones.

## Code quality

[Pylint](https://www.pylint.org/) was used in an effort to keep the program from looking like a an ASCII crime-scene. Its output below indicates that my OCD may have been triggered at some point:
```sh
$ pylint --version; pylint --enable-all-extensions *.py tests/
pylint 2.15.5
astroid 2.12.12
Python 3.8.10 (default, Jun 22 2022, 20:18:18)
[GCC 9.4.0]

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
```

Because I like being able to support old systems, the minimum Python version has been set to 3.3 in [.pylintrc](.pylintrc).

## Testing

At first, a rudimentary test was implemented with script [test.sh](test.sh), which just compares the output produced by boinc_exporter using known input file [misc/client_state_example.xml](misc/client_state_example.xml) with previously produced output [misc/boinc_exporter.prom](misc/boinc_exporter.prom). The test fails if any differences are found.

I then decided to act more like a grown-up and implemented a [pytest](https://docs.pytest.org/) suite, which you can find in the [tests](tests) directory. To run it, simply install pytest using your package manager or pip and then run 'pytest'. Example output is shown below:
```sh
$ pytest
============================= test session starts ==============================
platform linux -- Python 3.8.10, pytest-7.2.0, pluggy-1.0.0
rootdir: <redacted>/boinc_exporter
collected 36 items

tests/test_01_prometheus_metrics.py ...............                      [ 41%]
tests/test_02_boinc_exporter.py .....................                    [100%]

============================== 36 passed in 0.06s ==============================
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
I have been known to take a while to respond. Please don't take it personally if I do.

## License
[GPL version 3](https://www.gnu.org/licenses/)
You can find a full copy of the license in the file [COPYING](COPYING).
## Copyright
Thomas Venieris 2021-2022, all rights reserved.
