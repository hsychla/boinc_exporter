""" Tests for the boinc_exporter.py
"""

import getopt
import pytest
from boinc_exporter import BoincTextfileExporter

def test_workunit_name_compress():
    """ BoincTextfileExporter.workunit_name_compress tests
    """
    exp = BoincTextfileExporter()
    assert exp.workunit_name_compress('aaa') == 'aaa'
    assert exp.workunit_name_compress('p2030.20200521.G55.57-03.87.N.b3s0g0.00000_909_3') \
        == 'p2030.20200521.G55.57-03.87.N.b_b382'
    assert exp.workunit_name_compress('OONIProbe_RE_0D_T1668381740_3833_RR_0') \
        == 'OONIProbe_RE_0D_T1668381740_383_af44'
    assert exp.workunit_name_compress('OPN1_0124998_01510_0') == 'OPN1_0124998_01510_0'
    assert exp.workunit_name_compress( \
        'workspace2_hl14_OnErr_OnOct_NoBB_col_B1_s5_dp_0.000095__5__s__62.31_60.32__6.4_6.5' + \
        '__6__31_1_sixvf_boinc2167_0') == 'workspace2_hl14_OnErr_OnOct_NoB_94e7'
    assert exp.workunit_name_compress( \
        'MOF_P23_9mer_nb0016_initial_run_nmr_and_c3r3_solv46_c.10.7_0001_nres3_cell020_ASP_' + \
        '000002_SAVE_ALL_OUT_1058870_108_0') == 'MOF_P23_9mer_nb0016_initial_run_7954'
    assert exp.workunit_name_compress( \
        '2011061_pdb_0001_relaxed_0001_foldit_fold_SAVE_ALL_OUT_1058768_1447_0') \
        == '2011061_pdb_0001_relaxed_0001_f_1fe0'

def test_read_options():
    """ BoincTextfileExporter.read_options tests
    """
    exp = BoincTextfileExporter()
    exp.read_options(['boinc_exporter.py', '-f', 'misc/client_state_example.xml'])
    assert exp.client_state_xml_path == 'misc/client_state_example.xml'

    with pytest.raises(getopt.GetoptError):
        exp.read_options(['boinc_exporter.py', '-whatever'])

    exp.read_options(['boinc_exporter.py', '--no-netstats'])
    assert not exp.export['netstats']

def test_main_scrape_errors():
    """ BoincTextfileExporter.main scrape errors tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml'])

    assert exp.metrics.find('boinc_scrape_errors').get_text() == \
"""# HELP boinc_scrape_errors Number of errors encountered while parsing the boinc xml file
# TYPE boinc_scrape_errors gauge
boinc_scrape_errors 0
"""

def test_main_hostinfo_proc():
    """ BoincTextfileExporter.main hostinfo proc tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml' \
        , '--no-timestats', '--no-netstats', '--no-projects', '--no-workunits' \
        , '--no-results', '--no-active-tasks', '--no-user'
        ])

    assert exp.metrics.find('boinc_hostinfo_proc_cpus').get_text() == \
"""# HELP boinc_hostinfo_proc_cpus Number of processors, vendor and model details
# TYPE boinc_hostinfo_proc_cpus gauge
boinc_hostinfo_proc_cpus{vendor="ARM",model="BCM2835 [Impl 0x41 Arch 8 Variant 0x0 Part""" \
+ """ 0xd08 Rev 3]",features="fp asimd evtstrm crc32 cpuid"} 4
"""
    assert exp.metrics.find('boinc_hostinfo_proc_p_membw').get_text() == \
"""# TYPE boinc_hostinfo_proc_p_membw gauge
boinc_hostinfo_proc_p_membw 1000000000
"""
    assert exp.metrics.find('boinc_hostinfo_proc_calculated').get_text() == \
"""# HELP boinc_hostinfo_proc_calculated Calculated time of last client start (Unix timestamp)
# TYPE boinc_hostinfo_proc_calculated gauge
boinc_hostinfo_proc_calculated 1607459403.274438
"""
    assert exp.metrics.find('boinc_hostinfo_proc_vm_ext_disabled').get_text() == \
"""# HELP boinc_hostinfo_proc_vm_ext_disabled Processor VM extensions disabled (0) or enabled (1)
# TYPE boinc_hostinfo_proc_vm_ext_disabled gauge
boinc_hostinfo_proc_vm_ext_disabled 0
"""
    assert exp.metrics.find('boinc_hostinfo_usable_coprocs').get_text() == \
"""# HELP boinc_hostinfo_usable_coprocs Number of usable co-processors (like GPUs)
# TYPE boinc_hostinfo_usable_coprocs gauge
boinc_hostinfo_usable_coprocs 0
"""

def test_main_hostinfo_mem():
    """ BoincTextfileExporter.main hostinfo mem tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml' \
        , '--no-timestats', '--no-netstats', '--no-projects', '--no-workunits' \
        , '--no-results', '--no-active-tasks', '--no-user'
        ])

    assert exp.metrics.find('boinc_hostinfo_mem_nbytes').get_text() == \
"""# HELP boinc_hostinfo_mem_nbytes Available processor memory, in bytes
# TYPE boinc_hostinfo_mem_nbytes gauge
boinc_hostinfo_mem_nbytes 4294966272
"""
    assert exp.metrics.find('boinc_hostinfo_mem_cache').get_text() == \
"""# HELP boinc_hostinfo_mem_cache Size of the CPU cache
# TYPE boinc_hostinfo_mem_cache gauge
boinc_hostinfo_mem_cache -1
"""
    assert exp.metrics.find('boinc_hostinfo_mem_swap').get_text() == \
"""# HELP boinc_hostinfo_mem_swap Availalble swap space
# TYPE boinc_hostinfo_mem_swap gauge
boinc_hostinfo_mem_swap 4294963200
"""

def test_main_hostinfo_disk():
    """ BoincTextfileExporter.main hostinfo mem tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml' \
        , '--no-timestats', '--no-netstats', '--no-projects', '--no-workunits' \
        , '--no-results', '--no-active-tasks', '--no-user'
        ])

    assert exp.metrics.find('boinc_hostinfo_disk_total').get_text() == \
"""# HELP boinc_hostinfo_disk_total Host total disk size in bytes
# TYPE boinc_hostinfo_disk_total gauge
boinc_hostinfo_disk_total 8388009984
"""
    assert exp.metrics.find('boinc_hostinfo_disk_free').get_text() == \
"""# HELP boinc_hostinfo_disk_free Host free disk space in bytes
# TYPE boinc_hostinfo_disk_free gauge
boinc_hostinfo_disk_free 4422672384
"""

def test_main_timestats_frac():
    """ BoincTextfileExporter.main timestats frac tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml' \
        , '--no-hostinfo', '--no-netstats', '--no-projects', '--no-workunits' \
        , '--no-results', '--no-active-tasks', '--no-user'
        ])

    assert exp.metrics.find('boinc_timestats_on_frac').get_text() == \
"""# TYPE boinc_timestats_on_frac gauge
boinc_timestats_on_frac 0.999999
"""
    assert exp.metrics.find('boinc_timestats_connected_frac').get_text() == \
"""# TYPE boinc_timestats_connected_frac gauge
boinc_timestats_connected_frac -1
"""
    assert exp.metrics.find('boinc_timestats_cpunet_available_frac').get_text() == \
"""# TYPE boinc_timestats_cpunet_available_frac gauge
boinc_timestats_cpunet_available_frac 1
"""
    assert exp.metrics.find('boinc_timestats_active_frac').get_text() == \
"""# HELP boinc_timestats_active_frac Percentage of time active the last 24h (0.00-1.00)
# TYPE boinc_timestats_active_frac gauge
boinc_timestats_active_frac 1
"""
    assert exp.metrics.find('boinc_timestats_gpu_active_frac').get_text() == \
"""# HELP boinc_timestats_gpu_active_frac Percentage of time GPU was active the last 24h (0.00-1.00)
# TYPE boinc_timestats_gpu_active_frac gauge
boinc_timestats_gpu_active_frac 1
"""

def test_main_timestats():
    """ BoincTextfileExporter.main timestats uptime tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml' \
        , '--no-hostinfo', '--no-netstats', '--no-projects', '--no-workunits' \
        , '--no-results', '--no-active-tasks', '--no-user'
        ])

    assert exp.metrics.find('boinc_timestats_client_start_time').get_text() == \
"""# HELP boinc_timestats_client_start_time Datetime that client was started as a UNIX timestamp
# TYPE boinc_timestats_client_start_time gauge
boinc_timestats_client_start_time 1608283066.124689
"""
    assert exp.metrics.find('boinc_timestats_total_start_time').get_text() == \
"""# TYPE boinc_timestats_total_start_time gauge
boinc_timestats_total_start_time 1598818909.412089
"""
    assert exp.metrics.find('boinc_timestats_total_duration').get_text() == \
"""# TYPE boinc_timestats_total_duration gauge
boinc_timestats_total_duration 13031946.337382
"""
    assert exp.metrics.find('boinc_timestats_total_active_duration').get_text() == \
"""# TYPE boinc_timestats_total_active_duration gauge
boinc_timestats_total_active_duration 13031886.044696
"""
    assert exp.metrics.find('boinc_timestats_total_gpu_active_duration').get_text() == \
"""# TYPE boinc_timestats_total_gpu_active_duration gauge
boinc_timestats_total_gpu_active_duration 13031886.044696
"""
    assert exp.metrics.find('boinc_timestats_previous_uptime').get_text() == \
"""# TYPE boinc_timestats_previous_uptime gauge
boinc_timestats_previous_uptime 3578100.523477
"""
    assert exp.metrics.find('boinc_timestats_last_update').get_text() == \
"""# TYPE boinc_timestats_last_update gauge
boinc_timestats_last_update 1611861161.587162
"""

def test_main_netstats():
    """ BoincTextfileExporter.main netstats tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml' \
        , '--no-hostinfo', '--no-timestats', '--no-projects', '--no-workunits' \
        , '--no-results', '--no-active-tasks', '--no-user'
        ])

    assert exp.metrics.find('boinc_netstats_bwup').get_text() == \
"""# TYPE boinc_netstats_bwup gauge
boinc_netstats_bwup 114174.196609
"""
    assert exp.metrics.find('boinc_netstats_avg_up').get_text() == \
"""# TYPE boinc_netstats_avg_up gauge
boinc_netstats_avg_up 1338344.472027
"""
    assert exp.metrics.find('boinc_netstats_avg_time_up').get_text() == \
"""# TYPE boinc_netstats_avg_time_up gauge
boinc_netstats_avg_time_up 1611859947.414723
"""
    assert exp.metrics.find('boinc_netstats_bwdown').get_text() == \
"""# TYPE boinc_netstats_bwdown gauge
boinc_netstats_bwdown 2822519.412243
"""
    assert exp.metrics.find('boinc_netstats_avg_down').get_text() == \
"""# TYPE boinc_netstats_avg_down gauge
boinc_netstats_avg_down 25036124.634851
"""
    assert exp.metrics.find('boinc_netstats_avg_time_down').get_text() == \
"""# TYPE boinc_netstats_avg_time_down gauge
boinc_netstats_avg_time_down 1611859951.309099
"""

def test_main_projects_rpc():
    """ BoincTextfileExporter.main projects rpc tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml' \
        , '--no-hostinfo', '--no-timestats', '--no-netstats', '--no-workunits' \
        , '--no-results', '--no-active-tasks', '--no-user'
        ])

    assert exp.metrics.find('boinc_project_rpc_seqno').get_text() == \
"""# HELP boinc_project_rpc_seqno Number of Remote Procedure Calls
# TYPE boinc_project_rpc_seqno counter
boinc_project_rpc_seqno{project="Einstein@Home"} 131
boinc_project_rpc_seqno{project="LHC@home"} 1528
boinc_project_rpc_seqno{project="Rosetta@home"} 1084
boinc_project_rpc_seqno{project="Universe@Home"} 3769
boinc_project_rpc_seqno{project="World Community Grid"} 125
"""
    assert exp.metrics.find('boinc_project_nrpc_failures').get_text() == \
"""# HELP boinc_project_nrpc_failures Number of Remote Procedure Call failures
# TYPE boinc_project_nrpc_failures counter
boinc_project_nrpc_failures{project="Einstein@Home"} 0
boinc_project_nrpc_failures{project="LHC@home"} 0
boinc_project_nrpc_failures{project="Rosetta@home"} 0
boinc_project_nrpc_failures{project="Universe@Home"} 0
boinc_project_nrpc_failures{project="World Community Grid"} 0
"""
    assert exp.metrics.find('boinc_project_master_fetch_failures').get_text() == \
"""# TYPE boinc_project_master_fetch_failures counter
boinc_project_master_fetch_failures{project="Einstein@Home"} 0
boinc_project_master_fetch_failures{project="LHC@home"} 0
boinc_project_master_fetch_failures{project="Rosetta@home"} 0
boinc_project_master_fetch_failures{project="Universe@Home"} 2
boinc_project_master_fetch_failures{project="World Community Grid"} 0
"""

def test_main_projects_rpc_time():
    """ BoincTextfileExporter.main projects rpc time tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml' \
        , '--no-hostinfo', '--no-timestats', '--no-netstats', '--no-workunits' \
        , '--no-results', '--no-active-tasks', '--no-user'
        ])

    assert exp.metrics.find('boinc_project_min_rpc_time').get_text() == \
"""# HELP boinc_project_min_rpc_time Minimum time of next RPC (Unix timestamp)
# TYPE boinc_project_min_rpc_time gauge
boinc_project_min_rpc_time{project="Einstein@Home"} 1611941116.683004
boinc_project_min_rpc_time{project="LHC@home"} 1611855734.657958
boinc_project_min_rpc_time{project="Rosetta@home"} 1611859978.492197
boinc_project_min_rpc_time{project="Universe@Home"} 1611858340.277963
boinc_project_min_rpc_time{project="World Community Grid"} 1611841676.54197
"""
    assert exp.metrics.find('boinc_project_next_rpc_time').get_text() == \
"""# HELP boinc_project_next_rpc_time Time of next scheduled RPC, Unix timestamp
# TYPE boinc_project_next_rpc_time gauge
boinc_project_next_rpc_time{project="Einstein@Home"} 0
boinc_project_next_rpc_time{project="LHC@home"} 1611873728.657958
boinc_project_next_rpc_time{project="Rosetta@home"} 0
boinc_project_next_rpc_time{project="Universe@Home"} 1611861929.277963
boinc_project_next_rpc_time{project="World Community Grid"} 1612014476.54197
"""
    assert exp.metrics.find('boinc_project_last_rpc_time').get_text() == \
"""# HELP boinc_project_last_rpc_time Time of last RPC to project (Unix timestamp)
# TYPE boinc_project_last_rpc_time gauge
boinc_project_last_rpc_time{project="Einstein@Home"} 1611854716.683004
boinc_project_last_rpc_time{project="LHC@home"} 1611855728.657958
boinc_project_last_rpc_time{project="Rosetta@home"} 1611859947.492197
boinc_project_last_rpc_time{project="Universe@Home"} 1611858329.277963
boinc_project_last_rpc_time{project="World Community Grid"} 1611755276.54197
"""

def test_main_projects_credits():
    """ BoincTextfileExporter.main projects credits tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml' \
        , '--no-hostinfo', '--no-timestats', '--no-netstats', '--no-workunits' \
        , '--no-results', '--no-active-tasks', '--no-user'
        ])

    assert exp.metrics.find('boinc_project_host_total_credit').get_text() == \
"""# HELP boinc_project_host_total_credit Total credits earned by host per project
# TYPE boinc_project_host_total_credit gauge
boinc_project_host_total_credit{project="Einstein@Home"} 0
boinc_project_host_total_credit{project="LHC@home"} 23266.842788
boinc_project_host_total_credit{project="Rosetta@home"} 51629.167285
boinc_project_host_total_credit{project="Universe@Home"} 0
boinc_project_host_total_credit{project="World Community Grid"} 0
"""
    assert exp.metrics.find('boinc_project_host_expavg_credit').get_text() == \
"""# HELP boinc_project_host_expavg_credit Average credits earned by host per project
# TYPE boinc_project_host_expavg_credit gauge
boinc_project_host_expavg_credit{project="Einstein@Home"} 0
boinc_project_host_expavg_credit{project="LHC@home"} 487.761829
boinc_project_host_expavg_credit{project="Rosetta@home"} 315.045173
boinc_project_host_expavg_credit{project="Universe@Home"} 0
boinc_project_host_expavg_credit{project="World Community Grid"} 0
"""

def test_main_projects_rec():
    """ BoincTextfileExporter.main projects rec tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml' \
        , '--no-hostinfo', '--no-timestats', '--no-netstats', '--no-workunits' \
        , '--no-results', '--no-active-tasks', '--no-user'
        ])

    assert exp.metrics.find('boinc_project_rec').get_text() == \
"""# TYPE boinc_project_rec gauge
boinc_project_rec{project="Einstein@Home"} 0
boinc_project_rec{project="LHC@home"} 469.818992
boinc_project_rec{project="Rosetta@home"} 462.937779
boinc_project_rec{project="Universe@Home"} 0
boinc_project_rec{project="World Community Grid"} 0
"""
    assert exp.metrics.find('boinc_project_rec_time').get_text() == \
"""# TYPE boinc_project_rec_time gauge
boinc_project_rec_time{project="Einstein@Home"} 1611861166.648166
boinc_project_rec_time{project="LHC@home"} 1611861166.648166
boinc_project_rec_time{project="Rosetta@home"} 1611861166.648166
boinc_project_rec_time{project="Universe@Home"} 1611861166.648166
boinc_project_rec_time{project="World Community Grid"} 1611861166.648166
"""
    assert exp.metrics.find('boinc_project_resource_share').get_text() == \
"""# HELP boinc_project_resource_share The resources the user assigned to the project 0-100 (%)
# TYPE boinc_project_resource_share gauge
boinc_project_resource_share{project="Einstein@Home"} 100
boinc_project_resource_share{project="LHC@home"} 100
boinc_project_resource_share{project="Rosetta@home"} 100
boinc_project_resource_share{project="Universe@Home"} 100
boinc_project_resource_share{project="World Community Grid"} 100
"""

def test_main_projects_njobs():
    """ BoincTextfileExporter.main projects njobs tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml' \
        , '--no-hostinfo', '--no-timestats', '--no-netstats', '--no-workunits' \
        , '--no-results', '--no-active-tasks', '--no-user'
        ])

    assert exp.metrics.find('boinc_project_njobs_success').get_text() == \
"""# HELP boinc_project_njobs_success Total successful jobs per project
# TYPE boinc_project_njobs_success counter
boinc_project_njobs_success{project="Einstein@Home"} 0
boinc_project_njobs_success{project="LHC@home"} 489
boinc_project_njobs_success{project="Rosetta@home"} 586
boinc_project_njobs_success{project="Universe@Home"} 0
boinc_project_njobs_success{project="World Community Grid"} 0
"""
    assert exp.metrics.find('boinc_project_njobs_error').get_text() == \
"""# HELP boinc_project_njobs_error Total failed jobs per project
# TYPE boinc_project_njobs_error counter
boinc_project_njobs_error{project="Einstein@Home"} 0
boinc_project_njobs_error{project="LHC@home"} 7
boinc_project_njobs_error{project="Rosetta@home"} 17
boinc_project_njobs_error{project="Universe@Home"} 0
boinc_project_njobs_error{project="World Community Grid"} 0
"""
    assert exp.metrics.find('boinc_project_elapsed_time').get_text() == \
"""# HELP boinc_project_elapsed_time Elapsed time per project (CPU & GPU?)
# TYPE boinc_project_elapsed_time gauge
boinc_project_elapsed_time{project="Einstein@Home"} 0
boinc_project_elapsed_time{project="LHC@home"} 5335673.037763
boinc_project_elapsed_time{project="Rosetta@home"} 17595355.164261
boinc_project_elapsed_time{project="Universe@Home"} 0
boinc_project_elapsed_time{project="World Community Grid"} 0
"""

def test_main_projects_cpu():
    """ BoincTextfileExporter.main projects cpu tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml' \
        , '--no-hostinfo', '--no-timestats', '--no-netstats', '--no-workunits' \
        , '--no-results', '--no-active-tasks', '--no-user'
        ])

    assert exp.metrics.find('boinc_project_cpu_ec').get_text() == \
"""# HELP boinc_project_cpu_ec Earned CPU credits total
# TYPE boinc_project_cpu_ec counter
boinc_project_cpu_ec{project="Einstein@Home"} 0
boinc_project_cpu_ec{project="LHC@home"} 23031.559771
boinc_project_cpu_ec{project="Rosetta@home"} 65603.436473
boinc_project_cpu_ec{project="Universe@Home"} 0
boinc_project_cpu_ec{project="World Community Grid"} 0
"""
    assert exp.metrics.find('boinc_project_cpu_time').get_text() == \
"""# HELP boinc_project_cpu_time CPU time spent total in seconds
# TYPE boinc_project_cpu_time gauge
boinc_project_cpu_time{project="Einstein@Home"} 0
boinc_project_cpu_time{project="LHC@home"} 5345854.485532
boinc_project_cpu_time{project="Rosetta@home"} 17626516.706817
boinc_project_cpu_time{project="Universe@Home"} 0
boinc_project_cpu_time{project="World Community Grid"} 0
"""

def test_main_projects_gpu():
    """ BoincTextfileExporter.main projects gpu tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml' \
        , '--no-hostinfo', '--no-timestats', '--no-netstats', '--no-workunits' \
        , '--no-results', '--no-active-tasks', '--no-user'
        ])

    assert exp.metrics.find('boinc_project_gpu_ec').get_text() == \
"""# HELP boinc_project_gpu_ec Earned GPU credits total
# TYPE boinc_project_gpu_ec counter
boinc_project_gpu_ec{project="Einstein@Home"} 0
boinc_project_gpu_ec{project="LHC@home"} 0
boinc_project_gpu_ec{project="Rosetta@home"} 0
boinc_project_gpu_ec{project="Universe@Home"} 0
boinc_project_gpu_ec{project="World Community Grid"} 0
"""
    assert exp.metrics.find('boinc_project_gpu_time').get_text() == \
"""# HELP boinc_project_gpu_time GPU time spent total in seconds
# TYPE boinc_project_gpu_time gauge
boinc_project_gpu_time{project="Einstein@Home"} 0
boinc_project_gpu_time{project="LHC@home"} 0
boinc_project_gpu_time{project="Rosetta@home"} 0
boinc_project_gpu_time{project="Universe@Home"} 0
boinc_project_gpu_time{project="World Community Grid"} 0
"""

def test_main_projects_disk():
    """ BoincTextfileExporter.main projects disk tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml' \
        , '--no-hostinfo', '--no-timestats', '--no-netstats', '--no-workunits' \
        , '--no-results', '--no-active-tasks', '--no-user'
        ])

    assert exp.metrics.find('boinc_project_disk_usage').get_text() == \
"""# HELP boinc_project_disk_usage Disk used per project in bytes
# TYPE boinc_project_disk_usage gauge
boinc_project_disk_usage{project="Einstein@Home"} 369857
boinc_project_disk_usage{project="LHC@home"} 33899207
boinc_project_disk_usage{project="Rosetta@home"} 1745800306
boinc_project_disk_usage{project="Universe@Home"} 0
boinc_project_disk_usage{project="World Community Grid"} 2209629
"""
    assert exp.metrics.find('boinc_project_disk_share').get_text() == \
"""# HELP boinc_project_disk_share Disk available per project in bytes
# TYPE boinc_project_disk_share gauge
boinc_project_disk_share{project="Einstein@Home"} 892794184.26
boinc_project_disk_share{project="LHC@home"} 892794184.26
boinc_project_disk_share{project="Rosetta@home"} 4021676690
boinc_project_disk_share{project="Universe@Home"} 892794184.26
boinc_project_disk_share{project="World Community Grid"} 892794184.26
"""

def test_main_workunits():
    """ BoincTextfileExporter.main workunits tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml' \
        , '--no-hostinfo', '--no-timestats', '--no-netstats', '--no-projects' \
        , '--no-results', '--no-active-tasks', '--no-user'
        ])

    assert exp.metrics.find('boinc_workunit_count').get_text() == \
"""# HELP boinc_workunit_count Number of workunits per application
# TYPE boinc_workunit_count gauge
boinc_workunit_count{app="sixtrack",ver="50205"} 4
boinc_workunit_count{app="rosetta",ver="420"} 6
"""

def test_main_results():
    """ BoincTextfileExporter.main results tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml' \
        , '--no-hostinfo', '--no-timestats', '--no-netstats', '--no-projects' \
        , '--no-workunits', '--no-active-tasks', '--no-user'
        ])

    assert exp.metrics.find('boinc_result_earliest_deadline').get_text() == \
"""# HELP boinc_result_earliest_deadline Earliest result dead-line (Unix timestamp)
# TYPE boinc_result_earliest_deadline gauge
boinc_result_earliest_deadline{version_num="50205"} 1612277022
boinc_result_earliest_deadline{version_num="420"} 1611990961
"""
    assert exp.metrics.find('boinc_result_latest_received_time').get_text() == \
"""# HELP boinc_result_latest_received_time Latest received result time (Unix timestamp)
# TYPE boinc_result_latest_received_time gauge
boinc_result_latest_received_time{version_num="50205"} 1611761921.776064
boinc_result_latest_received_time{version_num="420"} 1611859947.492197
"""
    assert exp.metrics.find('boinc_result_exit_status').get_text() == \
"""# HELP boinc_result_exit_status Results amount per exit status
# TYPE boinc_result_exit_status gauge
boinc_result_exit_status{version_num="50205",exit_status="0"} 4
boinc_result_exit_status{version_num="420",exit_status="0"} 6
"""
    assert exp.metrics.find('boinc_result_state').get_text() == \
"""# HELP boinc_result_state Results amount per state
# TYPE boinc_result_state gauge
boinc_result_state{version_num="50205",state="2"} 4
boinc_result_state{version_num="420",state="2"} 6
"""

def test_main_active_tasks():
    """ BoincTextfileExporter.main active-tasks tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml' \
        , '--no-hostinfo', '--no-timestats', '--no-netstats', '--no-projects' \
        , '--no-workunits', '--no-results', '--no-user'
        ])

    assert exp.metrics.find('boinc_active_task_state').get_text() == \
'''# HELP boinc_active_task_state 0=Paused, 1=Executing, 9=Suspended
# TYPE boinc_active_task_state gauge
boinc_active_task_state{url="https://lhcathome.cern.ch/lhcathome/"''' \
    + ''',name="workspace2_hl14_OnErr_OnOct_NoB_94e7",version_num="50205"} 1
boinc_active_task_state{url="https://boinc.bakerlab.org/rosetta/"''' \
    + ''',name="MOF_P23_9mer_nb0016_initial_run_7954",version_num="420"} 1
boinc_active_task_state{url="https://boinc.bakerlab.org/rosetta/"''' \
    + ''',name="2011061_pdb_0001_relaxed_0001_f_1fe0",version_num="420"} 1
'''
    assert exp.metrics.find('boinc_active_task_chkp_elapsed_time').get_text() == \
'''# HELP boinc_active_task_chkp_elapsed_time Number of seconds elapsed since task became active
# TYPE boinc_active_task_chkp_elapsed_time gauge
boinc_active_task_chkp_elapsed_time{url="https://lhcathome.cern.ch/lhcathome/"''' \
    + ''',name="workspace2_hl14_OnErr_OnOct_NoB_94e7",version_num="50205"} 33500.071905
boinc_active_task_chkp_elapsed_time{url="https://boinc.bakerlab.org/rosetta/"''' \
    + ''',name="MOF_P23_9mer_nb0016_initial_run_7954",version_num="420"} 4256.75169
boinc_active_task_chkp_elapsed_time{url="https://boinc.bakerlab.org/rosetta/"''' \
    + ''',name="2011061_pdb_0001_relaxed_0001_f_1fe0",version_num="420"} 891.15706
'''
    assert exp.metrics.find('boinc_active_task_chkp_fraction_done').get_text() == \
'''# HELP boinc_active_task_chkp_fraction_done Percentage of task completed (0.0=0%, 1.0=100%)
# TYPE boinc_active_task_chkp_fraction_done gauge
boinc_active_task_chkp_fraction_done{url="https://lhcathome.cern.ch/lhcathome/"''' \
    + ''',name="workspace2_hl14_OnErr_OnOct_NoB_94e7",version_num="50205"} 0.685007
boinc_active_task_chkp_fraction_done{url="https://boinc.bakerlab.org/rosetta/"''' \
    + ''',name="MOF_P23_9mer_nb0016_initial_run_7954",version_num="420"} 0.322329
boinc_active_task_chkp_fraction_done{url="https://boinc.bakerlab.org/rosetta/"''' \
    + ''',name="2011061_pdb_0001_relaxed_0001_f_1fe0",version_num="420"} 0.012822
'''
    assert exp.metrics.find('boinc_active_task_page_fault_rate').get_text() == \
'''# TYPE boinc_active_task_page_fault_rate gauge
boinc_active_task_page_fault_rate{url="https://lhcathome.cern.ch/lhcathome/"''' \
    + ''',name="workspace2_hl14_OnErr_OnOct_NoB_94e7",version_num="50205"} 0
boinc_active_task_page_fault_rate{url="https://boinc.bakerlab.org/rosetta/"''' \
    + ''',name="MOF_P23_9mer_nb0016_initial_run_7954",version_num="420"} 0
boinc_active_task_page_fault_rate{url="https://boinc.bakerlab.org/rosetta/"''' \
    + ''',name="2011061_pdb_0001_relaxed_0001_f_1fe0",version_num="420"} 0
'''

def test_main_user():
    """ BoincTextfileExporter.main user tests
    """
    exp = BoincTextfileExporter()
    exp.main(['boinc_exporter.py', '-f', 'misc/client_state_example.xml' \
        , '--no-timestats', '--no-netstats', '--no-projects' \
        , '--no-workunits', '--no-results', '--no-active-tasks'
        ])

    assert exp.metrics.find('boinc_user_run_request').get_text() == \
"""# HELP boinc_user_run_request Process CPU workunits. 1=Always, 2=Auto, 3=Paused
# TYPE boinc_user_run_request gauge
boinc_user_run_request 2
"""
    assert exp.metrics.find('boinc_user_gpu_request').get_text() == \
"""# HELP boinc_user_gpu_request Process GPU workunits. 1=Always, 2=Auto, 3=Paused
# TYPE boinc_user_gpu_request gauge
boinc_user_gpu_request 2
"""
    assert exp.metrics.find('boinc_user_network_request').get_text() == \
"""# HELP boinc_user_network_request Use the network. 1=Always, 2=Auto, 3=Paused
# TYPE boinc_user_network_request gauge
boinc_user_network_request 2
"""
    assert exp.metrics.find('boinc_new_version_check_time').get_text() == \
"""# TYPE boinc_new_version_check_time gauge
boinc_new_version_check_time 0
"""
    assert exp.metrics.find('boinc_all_projects_list_check_time').get_text() == \
"""# TYPE boinc_all_projects_list_check_time gauge
boinc_all_projects_list_check_time 1611715405.289991
"""
    assert exp.metrics.find('boinc_platform').get_text() == \
'''# HELP boinc_platform Host platform, boinc client and OS version information (dummy metric)
# TYPE boinc_platform gauge
boinc_platform{name="aarch64-unknown-linux-gnu",major_ver="7",minor_ver="14",release="2"''' \
    + ',os_name="Linux Debian",os_version="Debian GNU/Linux 10 (buster) [5.4.51-v8+|libc' \
    + ''' 2.28 (Debian GLIBC 2.28-10)]",product_name="BCM2835"} 0
'''
