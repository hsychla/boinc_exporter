""" Tests for the simple Prometheus exporter library
"""

import pytest
from boinc_exporter import PrometheusLabelValue, PrometheusMetric

def test_prometheus_label_value_init():
    """ PrometheusLabelValue constructor tests
    """

    with pytest.raises(ValueError):
        val = PrometheusLabelValue({'777': 'value'})
    with pytest.raises(ValueError):
        val = PrometheusLabelValue({'1label': 'value'})
    with pytest.raises(ValueError):
        val = PrometheusLabelValue({'label-': 'value'})
    with pytest.raises(ValueError):
        val = PrometheusLabelValue({'label': None})
    with pytest.raises(ValueError):
        val = PrometheusLabelValue({'label': 0})
    with pytest.raises(ValueError):
        val = PrometheusLabelValue({'label': 'value'}, 'b')
    val = PrometheusLabelValue({'label': 'value'}, 1.0)
    assert val.value == 1.0

def test_prometheus_label_value_set():
    """ PrometheusLabelValue.set_value() tests
    """

    val = PrometheusLabelValue({})
    assert val.set_value(1) == 1
    assert not val.set_value('boo')

def test_prometheus_label_value_incr():
    """ PrometheusLabelValue.incr_value() tests
    """

    val = PrometheusLabelValue({})
    assert val.incr_value() == 1
    assert val.incr_value() == 2
    assert val.incr_value(2) == 4
    assert val.incr_value(1.2) == 5.2

def test_prometheus_label_value_set_if_bigger():
    """ PrometheusLabelValue.set_if_bigger() tests
    """

    val = PrometheusLabelValue({})
    assert val.set_if_bigger(5.1) is True
    assert val.value == 5.1
    assert not val.set_if_bigger(2.3)
    assert val.value == 5.1

def test_prometheus_label_value_set_if_smaller():
    """ PrometheusLabelValue.set_if_smaller() tests
    """

    val = PrometheusLabelValue({}, 7.0)
    assert val.set_if_smaller(5.1) is True
    assert val.value == 5.1
    assert not val.set_if_smaller(8.7)
    assert val.value == 5.1

def test_prometheus_label_value_addslashes():
    """ PrometheusLabelValue.addslashes() tests
    """

    assert PrometheusLabelValue.addslashes('aaa') == 'aaa'
    assert PrometheusLabelValue.addslashes('"aaa"') == '\\"aaa\\"'
    assert PrometheusLabelValue.addslashes("'aaa'") == '\\\'aaa\\\''
    assert PrometheusLabelValue.addslashes("c:\\users\\") == 'c:\\\\users\\\\'
    assert PrometheusLabelValue.addslashes("bbb\0") == 'bbb\\\0'
    assert PrometheusLabelValue.addslashes(None) is None

def test_prometheus_label_value_get_labels_text():
    """ PrometheusLabelValue.get_labels_text() tests
    """

    val = PrometheusLabelValue({ "label_name": "label_value"})
    assert val.get_labels_text() == '{label_name="label_value"}'
    val = PrometheusLabelValue({ "label_name": "label_value", "other_label": "other_value"})
    assert val.get_labels_text() == '{label_name="label_value",other_label="other_value"}'

def test_prometheus_metric_init():
    """ PrometheusMetric constructor tests
    """

    with pytest.raises(ValueError):
        metric = PrometheusMetric('222')
    with pytest.raises(ValueError):
        metric = PrometheusMetric('1metric')
    with pytest.raises(ValueError):
        metric = PrometheusMetric('metric-')
    metric = PrometheusMetric('some_metric')
    assert metric.name == 'some_metric'
    metric = PrometheusMetric('metric2')
    assert metric.name == 'metric2'

def test_prometheus_metric_set_type():
    """ PrometheusMetric.set_type() tests
    """

    metric = PrometheusMetric('some_metric')
    assert metric.set_type(PrometheusMetric.Gauge) is True
    assert metric.type == PrometheusMetric.Gauge
    assert metric.set_type(PrometheusMetric.Counter) is True
    assert metric.type == PrometheusMetric.Counter
    assert metric.set_type(PrometheusMetric.Histogram) is True
    assert metric.type == PrometheusMetric.Histogram
    assert metric.set_type(PrometheusMetric.Summary) is True
    assert metric.type == PrometheusMetric.Summary
    assert not metric.set_type(0)
    assert metric.type == PrometheusMetric.Summary
    assert not metric.set_type(5)
    assert metric.type == PrometheusMetric.Summary

def test_prometheus_metric_get_type_text():
    """ PrometheusMetric.get_type_text() tests
    """

    metric = PrometheusMetric('some_metric', PrometheusMetric.Gauge)
    assert metric.get_type_text() == 'gauge'
    metric = PrometheusMetric('some_metric', PrometheusMetric.Counter)
    assert metric.get_type_text() == 'counter'
    metric = PrometheusMetric('some_metric', PrometheusMetric.Histogram)
    assert metric.get_type_text() == 'histogram'
    metric = PrometheusMetric('some_metric', PrometheusMetric.Summary)
    assert metric.get_type_text() == 'summary'

def test_prometheus_metric_set_value():
    """ PrometheusMetric.set_value() tests
    """

    metric = PrometheusMetric('some_metric', PrometheusMetric.Gauge)
    assert metric.set_value(1) == 1
    assert metric.set_value(1.2) == 1.2
    assert not metric.set_value('boo')

def test_prometheus_metric_incr():
    """ PrometheusMetric.incr() tests
    """

    metric = PrometheusMetric('some_metric', PrometheusMetric.Gauge)
    assert metric.incr() == 1.0
    assert metric.incr(1.2) == 2.2
    assert not metric.incr('boo')

def test_prometheus_metric_label_values():
    """ PrometheusMetric.add_label_value(), PrometheusMetric.find_label_value() tests
    """

    metric = PrometheusMetric('some_metric', PrometheusMetric.Gauge)
    assert not metric.add_label_value({})
    assert metric.add_label_value({'node': 'node1'}) is True
    assert metric.add_label_value({'node': 'node2'}) is True
    assert metric.add_label_value({'node': 'node3'}, 3.3) is True
    # Already exists
    assert not metric.add_label_value({'node': 'node3'})
    # Search non-existing
    assert metric.find_label_value({'node': 'node4'}) is None
    # Search existing and check its value
    label_value = metric.find_label_value({'node': 'node3'})
    assert isinstance(label_value, PrometheusLabelValue) is True
    assert label_value.value == 3.3

def test_prometheus_metric_get_text_simple():
    """ PrometheusMetric.get_text(), PrometheusMetric.set_helptext() tests
    """

    metric = PrometheusMetric('some_metric', PrometheusMetric.Gauge)
    assert metric.get_text() == """# TYPE some_metric gauge
some_metric 0
"""
    metric.set_helptext('does something')
    assert metric.get_text() == """# HELP some_metric does something
# TYPE some_metric gauge
some_metric 0
"""
    metric.set_value(2.7)
    assert metric.get_text() == """# HELP some_metric does something
# TYPE some_metric gauge
some_metric 2.7
"""
    metric.set_value(3.0)
    assert metric.get_text() == """# HELP some_metric does something
# TYPE some_metric gauge
some_metric 3
"""

def test_prometheus_metric_get_text_label_values():
    """ PrometheusMetric.get_text() label values tests
    """

    metric = PrometheusMetric('project', PrometheusMetric.Gauge)
    metric.set_helptext('projects in progress')
    assert metric.add_label_value({'name': 'red'}, 7.1)
    assert metric.add_label_value({'name': 'green'}, 6.40)
    assert metric.add_label_value({'name': 'blue'}, 3.0)
    assert metric.get_text() == """# HELP project projects in progress
# TYPE project gauge
project{name="red"} 7.1
project{name="green"} 6.4
project{name="blue"} 3
"""
