#!/bin/sh
TMPFILE=/tmp/boinc_exporter.prom
python3 boinc_exporter.py -f misc/client_state_example.xml > ${TMPFILE}
diff misc/boinc_exporter.prom ${TMPFILE}

if [ $? -eq 0 ]
then
	echo 'Test successful'
else
	echo 'Test UNSUCCESSFUL'
fi
