#!/usr/bin/python3
""" boinc_exporter.py, copyright Thomas Venieris 2021-2022, all rights reserved.

boinc_exporter is a program that reads boinc's client_state.xml file and
outputs many of the statistics found in that file in the prometheus
exporter format

boinc_exporter is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation version 3 of the License.

boinc_exporter is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

import sys
import getopt
import xml.etree.ElementTree as ET
import hashlib
import re
# We include these statically for easier portability:
# from prometheus_metrics import PrometheusMetric, PrometheusMetricList

class PrometheusLabelValue:
    """Holds a prometheus metric value for specific labels
    """
    def __init__(self, labels, value=0.0):
        self.value = float(value)
        for label_name, label_value in labels.items():
            if not PrometheusMetric.is_valid_prometheus_name(label_name):
                raise ValueError("Invalid Prometheus label name: {}".format(label_name))
            if not isinstance(label_value, str):
                raise ValueError("Prometheus label value not a string: {}".format(label_value))
        self.labels = labels

    def set_value(self, value):
        """Sets the value of this metric

        Args:
            value (float): The new value
        """
        try:
            self.value = float(value)
        except ValueError:
            return False
        return self.value

    def incr_value(self, increment=1):
        """Increases the value of the metric

        Args:
            increment (int, optional): How much to increasy by. Defaults to 1.

        Returns:
            float: The new increased value
        """
        self.value = self.value + increment
        return self.value

    def set_if_bigger(self, value):
        """Set the metric to a new value, if it's bigger than the existing one.

        Args:
            value (float): The value to compare against the existing one

        Returns:
            bool: True if the new value was set
        """
        if value > self.value:
            self.value = float(value)
            return True
        return False

    def set_if_smaller(self, value):
        """Set the metric to a new value, if it's smaller than the existing one.

        Args:
            value (float): The value to compare against the existing one

        Returns:
            bool: True if the new value was set
        """
        if value < self.value:
            self.value = float(value)
            return True
        return False

    @staticmethod
    def addslashes(text):
        """Escapes text input so it can be put inside quotes

        Useful for prometheus metric labels

        Args:
            text (str): String to escape

        Returns:
            str: The escaped string
        """
        if text is not None:
            for i in ("\\", '"', "'", "\0"):
                if i in text:
                    text = text.replace(i, '\\'+i)
        return text

    def get_labels_text(self):
        """Returns our labels in the string format that prometheus expects

        Returns:
            str: prometheus labels
        """
        if self.labels == {}:
            return ''
        rlabels = []
        for key, value in self.labels.items():
            rlabels.append('{}="{}"'.format(key, __class__.addslashes(value)))
        return "{%s}" % (','.join(rlabels))

class PrometheusMetric:
    """Single prometheus metric class
    """
    Untyped = 0
    Gauge = 1
    Counter = 2
    Histogram = 3
    Summary = 4
    TypesText = [
        'untyped'
        , 'gauge'
        , 'counter'
        , 'histogram'
        , 'summary'
    ]

    def __init__(self, name, metric_type=2, helptext=None):
        if not self.set_name(name):
            raise ValueError("Invalid Prometheus metric name: {}".format(name))
        self.set_type(metric_type)
        self.helptext = helptext
        self.value = 0.0
        self.label_values = []

    @staticmethod
    def valid_types():
        """Returns a list with all valid prometheus metric types

        Returns:
            list: list of prometheus metric type IDs
        """
        return [
            __class__.Gauge,
            __class__.Counter,
            __class__.Histogram,
            __class__.Summary
        ]

    def set_type(self, metric_type):
        """Sets the type for this metric, after checkint that the type is valid.

        Args:
            metric_type (int): New metric type, like PrometheusMetric.Gauge \
                or PrometheusMetric.Counter

        Returns:
            bool: true if successful
        """
        if metric_type in __class__.valid_types():
            self.type = metric_type
            return True
        return False

    def get_type_text(self):
        """Returns the type of this prometheus metric as a string.

        Returns:
            str: textual representation of the type
        """
        return __class__.TypesText[self.type]

    def set_helptext(self, helptext):
        """Sets a description for the metric that is shown as help
        in the prometheus output.

        Args:
            helptext (str): The help-text to set
        """
        self.helptext = helptext

    @staticmethod
    def is_valid_prometheus_name(name):
        """ Checks if parameter is valid prometheus name, as used in metric and label names
        """

        return re.search('^[a-zA-Z_][a-zA-Z0-9_]*$', name) is not None

    def set_name(self, name):
        """ Sets the name of the metric, mostly used in the constructor
        """

        if not self.is_valid_prometheus_name(name):
            return False
        self.name = name
        return True

    def set_value(self, value):
        """Sets the metric value (always a float).
        Returns the value if succesful.

        Args:
            value (float): the value to set

        Returns:
            float: the new value
        """
        try:
            self.value = float(value)
        except ValueError:
            return False
        return self.value

    def incr(self, increment=1.0):
        """Increase the value of the metric by a fixed amount.

        Args:
            increment (float, optional): The amount by which to increase. Defaults to 1.

        Returns:
            int: The new value
        """
        try:
            self.value = self.value + increment
        except TypeError:
            return False
        return self.value

    def add_label_value(self, labels, value=0):
        """Add a value for specific labels

        Args:
            value (float): The value to set
            labels (dict): dictionary containing the labels

        Returns:
            bool: True if successful
        """
        if labels == {}:
            return False
        if self.find_label_value(labels):
            return False
        self.label_values.append(PrometheusLabelValue(labels, value))
        return True

    def find_label_value(self, labels):
        """Finds the label value object with the specified labels

        Args:
            labels (dict): The labels dict to search for

        Returns:
            PrometheusLabelValue: The label value object
        """
        for label_value in self.label_values:
            if label_value.labels == labels:
                return label_value
        return None

    def get_text(self):
        """Returns the metric in full prometheus text format, used for output

        Returns:
            string: full prometheus metric string
        """
        prom = ""
        if self.helptext is not None:
            prom = prom + "# HELP {} {}\n".format(self.name, self.helptext)
        prom = prom + "# TYPE {} {}\n".format(self.name, self.get_type_text())
        if self.label_values:
            for label_value in self.label_values:
                prom = prom + "{}{} {}\n".format( \
                    self.name, \
                    label_value.get_labels_text(), \
                    int(label_value.value) if label_value.value.is_integer() \
                    else label_value.value)
        else:
            prom = prom + "{} {}\n".format(self.name, \
                int(self.value) if self.value.is_integer() else self.value \
            )
        return prom

class PrometheusMetricList:
    """Holds and manages a list of PrometheusMetrics.
    """
    def __init__(self):
        self.metrics = {}

    def add(self, new_metric):
        """Adds a PrometheusMetric object to the list

        Args:
            new_metric (PrometheusMetric): The new metric to add
        """
        self.metrics[new_metric.name] = new_metric

    def add_list(self, new_metrics):
        """Adds a list of metric objects

        Args:
            new_metrics (list): list containing PrometheusMetric objects
        """
        for metric in new_metrics:
            self.add(metric)

    def find(self, metric_name):
        """Finds a metric in the list and returns it.

        Args:
            metric_name (str): The name of the metric to look for.

        Returns:
            PrometheusMetric: The object of the metric as contained in the list
        """
        return self.metrics[metric_name]

    def print_all(self):
        """Prints all the metrics as output for prometheus to consume.
        """
        for metric in self.metrics.values():
            print(metric.get_text())

class BoincTextfileExporter:
    """Boinc textfile exporter main class.
    """

    version = '1.2'
    client_state_xml_path_default = "/var/lib/boinc-client/client_state.xml"

    def __init__(self):
        self.metrics = PrometheusMetricList()
        self.client_state_xml_path = __class__.client_state_xml_path_default
        self.export = {
            'hostinfo': True,
            'timestats': True,
            'netstats': True,
            'projects': True,
            'workunits': True,
            'results': True,
            'active_tasks': True,
            'user': True
        }
        self.error_metric = PrometheusMetric('boinc_scrape_errors' \
            , PrometheusMetric.Gauge \
            , 'Number of errors encountered while parsing the boinc xml file')
        self.host_info = None

    @staticmethod
    def banner():
        """Print the program version and copyright.
        """
        print("""boinc_exporter.py version {}. Copyright Thomas Venieris 2021-2022
GNU General Public License version 3 release
""".format(__class__.version))

    @staticmethod
    def usage(argv):
        """Print the program usage parameters help screen
        """
        print("""Usage: {} -f <client_state.xml>

Parameters:
-h, --help                          This text
-f, --file <client_state.xml>       Specify an alternative path for the file
                                    client_state.xml. Default location is:
                                    {}

    --no-hostinfo                   Disables hostinfo statistics
    --no-timestats                  Disables time statistics
    --no-netstats                   Disables network statistics
    --no-projects                   Disables project statistics
    --no-workunits                  Disables workunit statistics
    --no-results                    Disables result statistics
    --no-active-tasks               Disables active task statistics
    --no-user                       Disables user and client statistics""".format( \
        argv[0], __class__.client_state_xml_path_default))

    def xml_find(self, node, tag):
        """Safely lookup a child tag in an XML node and increase the parsing
        error counter if it is not found.

        Args:
            node (Element): XML element to scan
            tag (str): Child tag name to look for

        Returns:
            Element: The found child element, or None
        """
        child = node.find(tag)
        if child is None:
            self.error_metric.incr()
            return None
        return child

    def xml_find_value(self, node, tag):
        """Safely lookup a child tag in an XML node and return its text value

        Args:
            node (Element): XML element to scan
            tag (str): Child tag name to look for

        Returns:
            str: The text value if found or "scrape_error"
        """
        child = self.xml_find(node, tag)
        if child is None:
            return 'scrape_error'
        return child.text

    def create_metrics(self):
        """Creates all the metric objects we are going to use,
        sets up types and help text.

        Returns:
            list: The list of the metrics created
        """
        self.metrics = PrometheusMetricList()

        self.metrics.add(self.error_metric)

        if self.export['hostinfo']:
            self.metrics.add_list([ \
                PrometheusMetric('boinc_hostinfo_proc_cpus', PrometheusMetric.Gauge \
                    , 'Number of processors, vendor and model details')
                # not useful:
                # , PrometheusMetric('boinc_hostinfo_proc_fpops', PrometheusMetric.Gauge)
                # , PrometheusMetric('boinc_hostinfo_proc_iops', PrometheusMetric.Gauge)
                , PrometheusMetric('boinc_hostinfo_proc_p_membw', PrometheusMetric.Gauge) \
                , PrometheusMetric('boinc_hostinfo_proc_calculated', PrometheusMetric.Gauge \
                    , "Calculated time of last client start (Unix timestamp)") \
                , PrometheusMetric('boinc_hostinfo_proc_vm_ext_disabled', PrometheusMetric.Gauge \
                    , "Processor VM extensions disabled (0) or enabled (1)") \
                , PrometheusMetric('boinc_hostinfo_mem_nbytes', PrometheusMetric.Gauge \
                    , "Available processor memory, in bytes") \
                , PrometheusMetric('boinc_hostinfo_mem_cache', PrometheusMetric.Gauge \
                    , 'Size of the CPU cache') \
                , PrometheusMetric('boinc_hostinfo_mem_swap', PrometheusMetric.Gauge \
                    , 'Availalble swap space') \
                , PrometheusMetric('boinc_hostinfo_disk_total', PrometheusMetric.Gauge \
                    , 'Host total disk size in bytes') \
                , PrometheusMetric('boinc_hostinfo_disk_free', PrometheusMetric.Gauge \
                     , 'Host free disk space in bytes') \
                , PrometheusMetric('boinc_hostinfo_usable_coprocs', PrometheusMetric.Gauge \
                    , 'Number of usable co-processors (like GPUs)') \
            ])

        if self.export['timestats']:
            self.metrics.add_list([ \
                PrometheusMetric('boinc_timestats_on_frac', PrometheusMetric.Gauge) \
                , PrometheusMetric('boinc_timestats_connected_frac', PrometheusMetric.Gauge) \
                , PrometheusMetric('boinc_timestats_cpunet_available_frac' \
                    , PrometheusMetric.Gauge) \
                , PrometheusMetric('boinc_timestats_active_frac', PrometheusMetric.Gauge \
                    , "Percentage of time active the last 24h (0.00-1.00)")
                , PrometheusMetric('boinc_timestats_gpu_active_frac', PrometheusMetric.Gauge \
                    , "Percentage of time GPU was active the last 24h (0.00-1.00)")
                , PrometheusMetric('boinc_timestats_client_start_time', PrometheusMetric.Gauge \
                    , "Datetime that client was started as a UNIX timestamp")
                , PrometheusMetric('boinc_timestats_total_start_time', PrometheusMetric.Gauge) \
                , PrometheusMetric('boinc_timestats_total_duration', PrometheusMetric.Gauge) \
                , PrometheusMetric('boinc_timestats_total_active_duration' \
                    , PrometheusMetric.Gauge) \
                , PrometheusMetric('boinc_timestats_total_gpu_active_duration' \
                    , PrometheusMetric.Gauge) \
                , PrometheusMetric('boinc_timestats_previous_uptime', PrometheusMetric.Gauge) \
                , PrometheusMetric('boinc_timestats_last_update', PrometheusMetric.Gauge) \
            ])

        if self.export['netstats']:
            self.metrics.add_list([ \
                PrometheusMetric('boinc_netstats_bwup', PrometheusMetric.Gauge) \
                , PrometheusMetric('boinc_netstats_avg_up', PrometheusMetric.Gauge) \
                , PrometheusMetric('boinc_netstats_avg_time_up', PrometheusMetric.Gauge) \
                , PrometheusMetric('boinc_netstats_bwdown', PrometheusMetric.Gauge) \
                , PrometheusMetric('boinc_netstats_avg_down', PrometheusMetric.Gauge) \
                , PrometheusMetric('boinc_netstats_avg_time_down', PrometheusMetric.Gauge) \
            ])

        if self.export['projects']:
            self.metrics.add_list([ \
                PrometheusMetric('boinc_project_rpc_seqno', PrometheusMetric.Counter \
                    , "Number of Remote Procedure Calls") \
                , PrometheusMetric('boinc_project_host_total_credit', PrometheusMetric.Gauge \
                    , "Total credits earned by host per project") \
                , PrometheusMetric('boinc_project_host_expavg_credit', PrometheusMetric.Gauge \
                    , "Average credits earned by host per project") \
                , PrometheusMetric('boinc_project_nrpc_failures', PrometheusMetric.Counter \
                    , "Number of Remote Procedure Call failures") \
                , PrometheusMetric('boinc_project_master_fetch_failures' \
                    , PrometheusMetric.Counter) \
                , PrometheusMetric('boinc_project_min_rpc_time', PrometheusMetric.Gauge \
                    , "Minimum time of next RPC (Unix timestamp)") \
                , PrometheusMetric('boinc_project_next_rpc_time', PrometheusMetric.Gauge \
                    , "Time of next scheduled RPC, Unix timestamp") \
                , PrometheusMetric('boinc_project_rec', PrometheusMetric.Gauge) \
                , PrometheusMetric('boinc_project_rec_time', PrometheusMetric.Gauge) \
                , PrometheusMetric('boinc_project_resource_share', PrometheusMetric.Gauge \
                    , "The resources the user assigned to the project 0-100 (%)") \
                , PrometheusMetric('boinc_project_njobs_success', PrometheusMetric.Counter \
                    , "Total successful jobs per project") \
                , PrometheusMetric('boinc_project_njobs_error', PrometheusMetric.Counter \
                    , "Total failed jobs per project") \
                , PrometheusMetric('boinc_project_elapsed_time', PrometheusMetric.Gauge \
                    , "Elapsed time per project (CPU & GPU?)") \
                , PrometheusMetric('boinc_project_last_rpc_time', PrometheusMetric.Gauge \
                    , "Time of last RPC to project (Unix timestamp)") \
                , PrometheusMetric('boinc_project_cpu_ec', PrometheusMetric.Counter \
                    , "Earned CPU credits total") \
                , PrometheusMetric('boinc_project_cpu_time', PrometheusMetric.Gauge \
                    , "CPU time spent total in seconds") \
                , PrometheusMetric('boinc_project_gpu_ec', PrometheusMetric.Counter \
                    , "Earned GPU credits total") \
                , PrometheusMetric('boinc_project_gpu_time', PrometheusMetric.Gauge \
                    , "GPU time spent total in seconds") \
                , PrometheusMetric('boinc_project_disk_usage', PrometheusMetric.Gauge \
                    , "Disk used per project in bytes") \
                , PrometheusMetric('boinc_project_disk_share', PrometheusMetric.Gauge \
                    , "Disk available per project in bytes") \
            ])
                # not useful: \
                # , PrometheusMetric('boinc_project_duration_correction_factor' \
                #   , PrometheusMetric.Gauge) \
                # , PrometheusMetric('boinc_project_rsc_backoff_time' \
                #   , PrometheusMetric.Gauge) \
                # , PrometheusMetric('boinc_project_rsc_backoff_interval' \
                #   , PrometheusMetric.Gauge) \

        if self.export['workunits']:
            self.metrics.add(PrometheusMetric('boinc_workunit_count' \
                , PrometheusMetric.Gauge \
                , "Number of workunits per application"))

        if self.export['results']:
            self.metrics.add_list([ \
                PrometheusMetric('boinc_result_earliest_deadline', PrometheusMetric.Gauge \
                    , "Earliest result dead-line (Unix timestamp)") \
                , PrometheusMetric('boinc_result_latest_received_time', PrometheusMetric.Gauge \
                    , "Latest received result time (Unix timestamp)") \
                , PrometheusMetric('boinc_result_exit_status', PrometheusMetric.Gauge \
                    , "Results amount per exit status") \
                , PrometheusMetric('boinc_result_state', PrometheusMetric.Gauge \
                    , "Results amount per state") \
            ])

        if self.export['active_tasks']:
            self.metrics.add_list([ \
                PrometheusMetric('boinc_active_task_state', PrometheusMetric.Gauge \
                    , "0=Paused, 1=Executing, 9=Suspended") \
                , PrometheusMetric('boinc_active_task_chkp_elapsed_time' \
                    , PrometheusMetric.Gauge \
                    , "Number of seconds elapsed since task became active") \
                , PrometheusMetric('boinc_active_task_chkp_fraction_done' \
                    , PrometheusMetric.Gauge \
                    , "Percentage of task completed (0.0=0%, 1.0=100%)")
                , PrometheusMetric('boinc_active_task_page_fault_rate' \
                    , PrometheusMetric.Gauge) \
            ])
            # not useful:
            # self.metrics.add(PrometheusMetric('boinc_active_task_chkp_cpu_time' \
            # , PrometheusMetric.Gauge))
            # not useful:
            # self.metrics.add(PrometheusMetric('boinc_active_task_current_cpu_time' \
            # , PrometheusMetric.Gauge))
            # not useful:
            # self.metrics.add(PrometheusMetric('boinc_active_task_once_ran_edf' \
            # , PrometheusMetric.Gauge))
            # not useful:
            # self.metrics.add(PrometheusMetric('boinc_active_task_swap_size' \
            # , PrometheusMetric.Gauge))
            # not useful:
            # self.metrics.add(PrometheusMetric('boinc_active_task_working_set_size' \
            # , PrometheusMetric.Gauge))
            # not useful:
            # self.metrics.add(PrometheusMetric('boinc_active_task_bytes_sent' \
            # , PrometheusMetric.Counter))
            # not useful: self.metrics.add(PrometheusMetric('boinc_active_task_bytes_received' \
            # , PrometheusMetric.Counter))

        if self.export['user']:
            self.metrics.add_list([ \
                PrometheusMetric('boinc_user_run_request', PrometheusMetric.Gauge \
                    , "Process CPU workunits. 1=Always, 2=Auto, 3=Paused") \
                , PrometheusMetric('boinc_user_gpu_request', PrometheusMetric.Gauge \
                    , "Process GPU workunits. 1=Always, 2=Auto, 3=Paused") \
                , PrometheusMetric('boinc_user_network_request', PrometheusMetric.Gauge \
                    , "Use the network. 1=Always, 2=Auto, 3=Paused") \
                , PrometheusMetric('boinc_new_version_check_time', PrometheusMetric.Gauge) \
                , PrometheusMetric('boinc_all_projects_list_check_time' \
                    , PrometheusMetric.Gauge) \
                , PrometheusMetric('boinc_platform', PrometheusMetric.Gauge \
                    , "Host platform, boinc client and OS version information (dummy metric)") \
            ])
            # not useful:
            # self.metrics.add(PrometheusMetric('boinc_user_run_prev_request' \
            # , PrometheusMetric.Gauge \
            # , "Process CPU workunits, previous setting. 1=Always, 2=Auto, 3=Paused"))
            # self.metrics.add(PrometheusMetric('boinc_user_gpu_prev_request' \
            # , PrometheusMetric.Gauge \
            # , "Process GPU workunits, previous setting. 1=Always, 2=Auto, 3=Paused"))
            # self.metrics.add(PrometheusMetric('boinc_client_version' \
            # , PrometheusMetric.Gauge))

        return self.metrics

    @staticmethod
    def workunit_name_compress(wu_name, max_chars=36):
        """Shorten a boinc work unit's name to max_chars characters or less

        Work-unit names can routinely be over 100 characters long. Storing them
        as they are in prometheus as labels increases prometheus disk usage
        and can affect prometheus performance. They can also easily break
        grafana dashboards that show them.

        This function shortens the name if over the limit and prevents
        name collisions by appending as a suffix a 4-byte hash of the long name

        The 36 character limit was chosen simply because that's how big a UUID
        with the dashes included.

        Args:
            wu_name (str): Name of the work unit
            max_chars (int, optional): Maximum size of the new name. Defaults to 36.

        Returns:
            str: The new work-unit name
        """
        if len(wu_name) <= max_chars:
            # The name is already short enough
            return wu_name
        md5 = hashlib.md5()
        md5.update(wu_name.encode())
        wu_hash = md5.hexdigest()[0:4]
        return "{}_{}".format(wu_name[0:max_chars-5], wu_hash)

    def read_options(self, argv):
        """ Read user specified options from command-line """
        opts, args = getopt.getopt(argv[1:], "hf:", ["help", "file=", "no-hostinfo" \
            , "no-timestats", "no-netstats", "no-projects", "no-workunits", "no-results" \
            , "no-active-tasks", "no-user"])
        # pylint silencer
        if args == []:
            pass
        for opt, arg in opts:
            if opt in {"-h", "--help"}:
                __class__.banner()
                __class__.usage(argv)
                sys.exit()
        for opt, arg in opts:
            if opt in {"-f", "--file"}:
                self.client_state_xml_path = arg
            else:
                for export_str in ('hostinfo', 'timestats', 'netstats', 'projects', \
                    'workunits', 'results', 'active-tasks', 'user'):
                    if opt == "--no-{}".format(export_str):
                        self.export[export_str] = False

    def process_hostinfo(self, xml_root):
        """Read hostinfo metrics from XML file"""

        self.host_info = self.xml_find(xml_root, 'host_info') if self.export['hostinfo'] else None
        if self.host_info is not None:
            xml_tag_to_metric_name = {
                'p_membw': 'boinc_hostinfo_proc_p_membw' \
                , 'p_calculated': 'boinc_hostinfo_proc_calculated' \
                , 'p_vm_extensions_disabled': 'boinc_hostinfo_proc_vm_ext_disabled' \
                , 'm_nbytes': 'boinc_hostinfo_mem_nbytes' \
                , 'm_cache': 'boinc_hostinfo_mem_cache' \
                , 'm_swap': 'boinc_hostinfo_mem_swap' \
                , 'd_total': 'boinc_hostinfo_disk_total' \
                , 'd_free': 'boinc_hostinfo_disk_free' \
                , 'n_usable_coprocs': 'boinc_hostinfo_usable_coprocs' \
                # not useful:
                # , 'p_fpops': 'boinc_hostinfo_proc_fpops' \
                # , 'p_iops': 'boinc_hostinfo_proc_iops' \
            }
            for elem in self.host_info:
                if elem.tag == 'p_ncpus':
                    cpu_labels = {}
                    tag = self.xml_find(self.host_info, 'p_vendor')
                    if tag is not None:
                        cpu_labels["vendor"] = tag.text
                    tag = self.xml_find(self.host_info, "p_model")
                    if tag is not None:
                        cpu_labels["model"] = tag.text
                    tag = self.xml_find(self.host_info, "p_features")
                    if tag is not None:
                        cpu_labels["features"] = tag.text

                    self.metrics.find('boinc_hostinfo_proc_cpus').add_label_value( \
                        cpu_labels, elem.text)
                elif elem.tag in xml_tag_to_metric_name:
                    self.metrics.find(xml_tag_to_metric_name[elem.tag]).set_value(elem.text)

    def process_timestats(self, xml_root):
        """Read timestats metrics from XML file"""
        if not self.export['timestats']:
            return False
        time_stats = self.xml_find(xml_root, "time_stats")
        if time_stats is None:
            return False
        xml_tag_to_metric_name = {
            'on_frac': 'boinc_timestats_on_frac' \
            , 'connected_frac': 'boinc_timestats_connected_frac' \
            , 'cpu_and_network_available_frac': 'boinc_timestats_cpunet_available_frac' \
            , 'active_frac': 'boinc_timestats_active_frac' \
            , 'gpu_active_frac': 'boinc_timestats_gpu_active_frac' \
            , 'client_start_time': 'boinc_timestats_client_start_time' \
            , 'total_start_time': 'boinc_timestats_total_start_time' \
            , 'total_duration': 'boinc_timestats_total_duration' \
            , 'total_active_duration': 'boinc_timestats_total_active_duration' \
            , 'total_gpu_active_duration': 'boinc_timestats_total_gpu_active_duration' \
            , 'previous_uptime': 'boinc_timestats_previous_uptime' \
            , 'last_update': 'boinc_timestats_last_update' \
        }
        for elem in time_stats:
            if elem.tag in xml_tag_to_metric_name:
                self.metrics.find(xml_tag_to_metric_name[elem.tag]).set_value(elem.text)
        return True

    def process_netstats(self, xml_root):
        """Read netstats metrics from XML file"""
        if not self.export['netstats']:
            return False
        net_stats = self.xml_find(xml_root, "net_stats")
        if net_stats is None:
            return False
        xml_tag_to_metric_name = {
            'bwup': 'boinc_netstats_bwup' \
            , 'avg_up': 'boinc_netstats_avg_up' \
            , 'avg_time_up': 'boinc_netstats_avg_time_up' \
            , 'bwdown': 'boinc_netstats_bwdown' \
            , 'avg_down': 'boinc_netstats_avg_down' \
            , 'avg_time_down': 'boinc_netstats_avg_time_down' \
        }
        for elem in net_stats:
            if elem.tag in xml_tag_to_metric_name:
                self.metrics.find(xml_tag_to_metric_name[elem.tag]).set_value(elem.text)
        return True

    def process_projects(self, xml_root):
        """Read project metrics from XML file"""
        if not self.export['projects']:
            return False
        xml_tag_to_metric_name = {
            'rpc_seqno': 'boinc_project_rpc_seqno' \
            , 'host_total_credit': 'boinc_project_host_total_credit' \
            , 'host_expavg_credit': 'boinc_project_host_expavg_credit' \
            , 'nrpc_failures': 'boinc_project_nrpc_failures' \
            , 'master_fetch_failures': 'boinc_project_master_fetch_failures' \
            , 'min_rpc_time': 'boinc_project_min_rpc_time' \
            , 'next_rpc_time': 'boinc_project_next_rpc_time' \
            , 'rec': 'boinc_project_rec' \
            , 'rec_time': 'boinc_project_rec_time' \
            , 'resource_share': 'boinc_project_resource_share' \
            , 'njobs_success': 'boinc_project_njobs_success' \
            , 'njobs_error': 'boinc_project_njobs_error' \
            , 'elapsed_time': 'boinc_project_elapsed_time' \
            , 'last_rpc_time': 'boinc_project_last_rpc_time' \
            , 'cpu_ec': 'boinc_project_cpu_ec' \
            , 'cpu_time': 'boinc_project_cpu_time' \
            , 'gpu_ec': 'boinc_project_gpu_ec' \
            , 'gpu_time': 'boinc_project_gpu_time' \
            , 'disk_usage': 'boinc_project_disk_usage' \
            , 'disk_share': 'boinc_project_disk_share' \
            # Not useful: \
            # , 'duration_correction_factor': 'boinc_project_duration_correction_factor' \
        }
        for project in xml_root.findall("project"):
            project_name = self.xml_find_value(project, "project_name")
            for elem in project:
                if elem.tag in xml_tag_to_metric_name:
                    self.metrics.find(xml_tag_to_metric_name[elem.tag]).add_label_value( \
                        {"project": project_name}, elem.text)

            #rsc_backoff_times = project.findall("rsc_backoff_time")
            #for rsc_backoff_time in rsc_backoff_times:
            #    metrics.find('boinc_project_rsc_backoff_time').add_label_value(
            #        {
            #            "project": project_name,
            #            "name": self.xml_find_value(rsc_backoff_time, 'name')
            #    }, elem.text)
            #rsc_backoff_intervals = project.findall("rsc_backoff_interval")
            #for rsc_backoff_interval in rsc_backoff_intervals:
            #    metrics.find('boinc_project_rsc_backoff_interval').add_label_value(
            #        {
            #            "project": project_name,
            #            "name": self.xml_find_value(rsc_backoff_interval, 'name')
            #    }, elem.text)
        return True

    def process_workunits(self, xml_root):
        """Read work-unit metrics from XML file"""
        if not self.export['workunits']:
            return False
        metric = self.metrics.find('boinc_workunit_count')
        for workunit in xml_root.findall("workunit"):
            app_labels = { \
                "app": self.xml_find_value(workunit, 'app_name') \
                , "ver": self.xml_find_value(workunit, 'version_num') \
            }
            label_value = metric.find_label_value(app_labels)
            if label_value is not None:
                label_value.incr_value()
            else:
                metric.add_label_value(app_labels, 1)
        return True

    def process_results(self, xml_root):
        """Read task result metrics from XML file"""
        if not self.export['results']:
            return False
        xml_tag_to_metric_name = {
            'exit_status': 'boinc_result_exit_status' \
            , 'state': 'boinc_result_state' \
        }
        for result in xml_root.findall("result"):
            app_labels = {"version_num": self.xml_find_value(result, 'version_num')}

            metric = self.metrics.find('boinc_result_earliest_deadline')
            value = float(self.xml_find_value(result, 'report_deadline'))
            label_value = metric.find_label_value(app_labels)
            if label_value is not None:
                label_value.set_if_smaller(value)
            else:
                metric.add_label_value(app_labels, value)

            metric = self.metrics.find('boinc_result_latest_received_time')
            value = float(self.xml_find_value(result, 'received_time'))
            label_value = metric.find_label_value(app_labels)
            if label_value is not None:
                label_value.set_if_bigger(value)
            else:
                metric.add_label_value(app_labels, value)

            for xml_tag, metric_name in xml_tag_to_metric_name.items():
                metric = self.metrics.find(metric_name)
                my_labels = app_labels.copy()
                my_labels[xml_tag] = self.xml_find_value(result, xml_tag)
                value = metric.find_label_value(my_labels)
                if value is not None:
                    value.incr_value()
                else:
                    metric.add_label_value(my_labels, 1)
        return True

    def process_active_tasks(self, xml_root):
        """Read active task metrics from XML file"""
        if not self.export['active_tasks']:
            return False
        xml_tag_to_metric_name = {
            "active_task_state": "boinc_active_task_state" \
            , "checkpoint_elapsed_time": "boinc_active_task_chkp_elapsed_time" \
            , "checkpoint_fraction_done": "boinc_active_task_chkp_fraction_done" \
            , "page_fault_rate": "boinc_active_task_page_fault_rate" \
            # , "checkpoint_cpu_time": "boinc_active_task_chkp_cpu_time" \
            # , "current_cpu_time": "boinc_active_task_current_cpu_time" \
            # , "once_ran_edf": "boinc_active_task_once_ran_edf" \
            # , "swap_size": "boinc_active_task_swap_size" \
            # , "working_set_size": "boinc_active_task_working_set_size" \
            # , "bytes_sent": "boinc_active_task_bytes_sent" \
            # , "bytes_received": "boinc_active_task_bytes_received" \
        }
        for task in xml_root.findall("active_task_set/active_task"):
            task_labels = {
                "url": self.xml_find_value(task, 'project_master_url'),
                "name": __class__.workunit_name_compress( \
                    self.xml_find_value(task, 'result_name')),
                "version_num": self.xml_find_value(task, 'app_version_num')
            }
            for xml_tag, metric_name in xml_tag_to_metric_name.items(): \
                self.metrics.find(metric_name).add_label_value( \
                    task_labels, self.xml_find_value(task, xml_tag))
        return True

    def process_user(self, xml_root):
        """Read user metrics from XML file"""

        if self.export['user']:
            xml_tag_to_metric_name = {
                "user_run_request": "boinc_user_run_request" \
                , "user_gpu_request": "boinc_user_gpu_request" \
                , "user_network_request": "boinc_user_network_request" \
                , "new_version_check_time": "boinc_new_version_check_time" \
                , "all_projects_list_check_time": "boinc_all_projects_list_check_time" \
                # , "user_run_prev_request": "boinc_user_run_prev_request" \
                # , "user_gpu_prev_request": "boinc_user_gpu_prev_request" \
            }
            for xml_tag, metric_name in xml_tag_to_metric_name.items():
                self.metrics.find(metric_name).set_value(
                    self.xml_find_value(xml_root, xml_tag))

            platform_labels = {
                "name": self.xml_find_value(xml_root, "platform_name") \
                , "major_ver": self.xml_find_value(xml_root, "core_client_major_version") \
                , "minor_ver": self.xml_find_value(xml_root, "core_client_minor_version") \
                , "release": self.xml_find_value(xml_root, "core_client_release")
            }
            if self.host_info is not None:
                platform_labels["os_name"] = self.xml_find_value(self.host_info, "os_name")
                platform_labels["os_version"] = self.xml_find_value(self.host_info, "os_version")
                platform_labels["product_name"] = self.xml_find_value(self.host_info, \
                    "product_name")
            self.metrics.find('boinc_platform').add_label_value(platform_labels)

    def main(self, argv):
        """boinc_exporter main function

        Args:
            argv (list): system argv
        """
        try:
            self.read_options(argv)
        except getopt.GetoptError:
            __class__.banner()
            __class__.usage(argv)
            sys.exit(2)

        self.create_metrics()

        try:
            # create element tree object
            tree = ET.parse(self.client_state_xml_path)
        except FileNotFoundError:
            self.error_metric.add_label_value( \
                {"msg": "File '{}' not found".format(self.client_state_xml_path)}, 1)
            print(self.error_metric.get_text())
            sys.exit()
        except ET.ParseError:
            self.error_metric.add_label_value( \
                {"msg": "Failed to parse XML file '{}'".format(self.client_state_xml_path)}, 1)
            print(self.error_metric.get_text())
            sys.exit()

        xml_root = tree.getroot()
        self.process_hostinfo(xml_root)
        self.process_user(xml_root)
        self.process_timestats(xml_root)
        self.process_netstats(xml_root)
        self.process_projects(xml_root)
        self.process_workunits(xml_root)
        self.process_results(xml_root)
        self.process_active_tasks(xml_root)
        self.metrics.print_all()

if __name__ == "__main__":
    EXPORTER = BoincTextfileExporter()
    EXPORTER.main(sys.argv)
